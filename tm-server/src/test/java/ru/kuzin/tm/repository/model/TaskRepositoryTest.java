package ru.kuzin.tm.repository.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.model.IProjectRepository;
import ru.kuzin.tm.api.repository.model.ITaskRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.model.IUserService;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.migration.AbstractSchemeTest;
import ru.kuzin.tm.model.Project;
import ru.kuzin.tm.model.Task;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;
import ru.kuzin.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.kuzin.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ITaskRepository repository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private Project project;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    public static User USER_1;

    public static User USER_2;

    public static long USER_ID_COUNTER = 0;

    public static EntityManager entityManager;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("task_rep_mod_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("task_rep_mod_usr_2_" + USER_ID_COUNTER, "1");
        repository = new TaskRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        taskList = new ArrayList<>();
        project = new Project();
        project.setName("Test_project");
        project.setUser(USER_1);
        entityManager.getTransaction().begin();
        projectRepository.add(USER_1.getId(), project);
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProject(project);
            task.setUser(USER_1);
            repository.add(USER_1.getId(), task);
            task.setUser(USER_1);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUser(USER_2);
            repository.add(USER_2.getId(), task);
            task.setUser(USER_2);
            taskList.add(task);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void clearAfter() {
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Test
    public void testAddTaskPositive() {
        Task task = new Task();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        task.setUser(USER_1);
        repository.add(USER_1.getId(), task);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_1.getId()));
        repository.clear(USER_1.getId());
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_2.getId()));
        repository.clear(USER_2.getId());
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_1.getId(), UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_2.getId(), UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            final Task foundTask = repository.findOneById(task.getUser().getId(), task.getId());
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task.getId(), foundTask.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_1.getId(), UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_2.getId(), UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            Assert.assertTrue(repository.existsById(task.getUser().getId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_1.getId(), 9999));
        Assert.assertNull(repository.findOneByIndex(USER_2.getId(), 9999));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_1.getId(), i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i).getId(), foundTask.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_2.getId(), i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5).getId(), foundTask.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<Task> tasks = repository.findAll(USER_1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_2.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i - 5).getId(), taskList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<Task> tasks = repository.findAll(USER_1.getId(), CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_2.getId(), CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<Task> tasks = repository.findAll(USER_1.getId(), STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_2.getId(), STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<Task> tasks = repository.findAll(USER_1.getId(), NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_2.getId(), NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_1.getId()));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.removeById(USER_1.getId(), taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_1.getId(), taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_2.getId()));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.removeById(USER_2.getId(), taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_2.getId(), taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_1.getId()));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.remove(USER_1.getId(), taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_2.getId()));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.remove(USER_2.getId(), taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

    @Test
    public void testTaskFindByProjectId() {
        List<Task> tasks = repository.findAllByProjectId(USER_1.getId(), project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}