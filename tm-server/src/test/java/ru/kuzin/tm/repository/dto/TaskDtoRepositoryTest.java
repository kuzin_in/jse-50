package ru.kuzin.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.dto.IProjectDtoRepository;
import ru.kuzin.tm.api.repository.dto.ITaskDtoRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.dto.IUserDtoService;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.dto.model.TaskDTO;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.migration.AbstractSchemeTest;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;
import ru.kuzin.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.kuzin.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ITaskDtoRepository repository;

    @NotNull
    private IProjectDtoRepository projectRepository;

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(propertyService, connectionService);

    public static String USER_ID_1;

    public static String USER_ID_2;

    public static long USER_ID_COUNTER;

    public static EntityManager entityManager;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("task_rep_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("task_rep_usr_2_" + USER_ID_COUNTER, "1").getId();
        repository = new TaskDtoRepository(entityManager);
        projectRepository = new ProjectDtoRepository(entityManager);
        taskList = new ArrayList<>();
        project = new ProjectDTO();
        project.setName("Test_project");
        project.setUserId(USER_ID_1);
        entityManager.getTransaction().begin();
        projectRepository.add(USER_ID_1, project);
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(project.getId());
            repository.add(USER_ID_1, task);
            task.setUserId(USER_ID_1);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            repository.add(USER_ID_2, task);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void clearAfter() {
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        repository.clear(USER_ID_1);
        repository.clear(USER_ID_2);
        projectRepository.clear(USER_ID_1);
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Test
    public void testAddTaskPositive() {
        TaskDTO task = new TaskDTO();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        repository.add(USER_ID_1, task);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            final TaskDTO foundTask = repository.findOneById(task.getUserId(), task.getId());
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task.getId(), foundTask.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            Assert.assertTrue(repository.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_1, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i).getId(), foundTask.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_2, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5).getId(), foundTask.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<TaskDTO> tasks = repository.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i - 5).getId(), taskList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, STATUS_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<TaskDTO> tasks = repository.findAll(USER_ID_1, NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, NAME_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.removeById(USER_ID_1, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_1, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.removeById(USER_ID_2, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_2, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.remove(USER_ID_1, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.remove(USER_ID_2, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testTaskFindByProjectId() {
        List<TaskDTO> tasks = repository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}