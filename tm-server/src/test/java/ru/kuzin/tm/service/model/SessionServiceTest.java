package ru.kuzin.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.model.ISessionService;
import ru.kuzin.tm.api.service.model.IUserService;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.exception.field.IdEmptyException;
import ru.kuzin.tm.exception.field.IndexIncorrectException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.migration.AbstractSchemeTest;
import ru.kuzin.tm.model.Session;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.kuzin.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static ISessionService sessionService;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private static IUserService userService;

    public static User USER;

    public static long USER_ID_COUNTER;

    @NotNull
    private List<User> userList = new ArrayList<>();


    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sessionService = new SessionService(connectionService);
        userService = new UserService(propertyService, connectionService);
    }

    @Before
    public void init() {
        sessionList = new ArrayList<>();
        userList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            USER_ID_COUNTER++;
            USER = userService.create("session_serv_usr_" + USER_ID_COUNTER, "1");
            userList.add(USER);
        }
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userList.get(i));
            session.setRole(Role.USUAL);
            sessionService.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void closeConnection() {
        userService.clear();
    }

    @Test
    public void testClearPositive() {
        for (final User user : userList) {
            Assert.assertEquals(1, sessionService.findAll(user.getId()).size());
            sessionService.clear(user.getId());
            Assert.assertEquals(0, sessionService.findAll(user.getId()).size());
        }
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Session> sessions;
        for (final User user : userList) {
            sessions = sessionService.findAll(user.getId());
            Assert.assertNotNull(sessions);
            for (final Session session : sessionList) {
                if (session.getUser().getId().equals(user.getId())) {
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getUser().getId().equals(m.getUser().getId()))
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .findFirst()
                                    .orElse(null)
                    );
                }
            }
        }
        for (@NotNull final User user : userList) {
            sessionService.clear(user.getId());
        }
        ;
        sessions = sessionService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), sessions);
    }

    @Test
    public void testAddSessionNegative() {
        @NotNull final Session session = new Session();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(NULLABLE_USER_ID, session));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(EMPTY_USER_ID, session));
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull Session session = new Session();
        USER_ID_COUNTER++;
        session.setUser(userService.create("session_serv_usr_" + USER_ID_COUNTER, "1"));
        session.setRole(Role.USUAL);
        sessionService.add(userList.get(0).getId(), session);
        Assert.assertEquals(2, sessionService.findAll(userList.get(0).getId()).size());
        session.setId(UUID.randomUUID().toString());
        Assert.assertNotNull(sessionService.add(session));
    }

    @Test
    public void testAdd() {
        @NotNull final Session session = new Session();
        session.setUser(userList.get(0));
        session.setRole(Role.USUAL);
        for (@NotNull final User user : userList) {
            sessionService.clear(user.getId());
        }
        ;
        sessionService.add(session);
        sessionList.add(0, session);
        Assert.assertEquals(1, sessionService.findAll(userList.get(0).getId()).size());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(EMPTY_SESSION_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        Assert.assertFalse(sessionService.existsById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertTrue(sessionService.existsById(session.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final Session session : sessionList) {
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getUser().getId(), session.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (final User user : userList) {
            Assert.assertEquals(sessionList.get(userList.indexOf(user)).getId(), sessionService.findOneByIndex(user.getId(), 1).getId());
        }
    }

    @Test
    public void testGetSizeNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void testRemovePositive() {
        for (final Session session : sessionList) {
            sessionService.remove(session);
            Assert.assertFalse(sessionService.findAll(session.getUser().getId()).contains(session));
        }
        for (final User user : userList)
            Assert.assertEquals(0, sessionService.findAll(user.getId()).size());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(NULLABLE_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(EMPTY_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Session session : sessionList) {
            sessionService.removeById(session.getUser().getId(), session.getId());
            Assert.assertFalse(sessionService.findAll(session.getUser().getId()).contains(session));
            Assert.assertEquals(0, sessionService.findAll(session.getUser().getId()).size());
        }
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
    }

    @Test
    public void testRemoveByIndexPositive() {
        for (final User user : userList) {
            Assert.assertEquals(1, sessionService.findAll(user.getId()).size());
            sessionService.removeByIndex(user.getId(), 1);
            Assert.assertEquals(0, sessionService.findAll(user.getId()).size());
        }
    }

}