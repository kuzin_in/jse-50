package ru.kuzin.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.model.IProjectRepository;
import ru.kuzin.tm.api.repository.model.ITaskRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.model.IUserService;
import ru.kuzin.tm.exception.entity.ProjectNotFoundException;
import ru.kuzin.tm.exception.entity.TaskNotFoundException;
import ru.kuzin.tm.exception.field.ProjectIdEmptyException;
import ru.kuzin.tm.exception.field.TaskIdEmptyException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.migration.AbstractSchemeTest;
import ru.kuzin.tm.model.Project;
import ru.kuzin.tm.model.Task;
import ru.kuzin.tm.model.User;
import ru.kuzin.tm.repository.model.ProjectRepository;
import ru.kuzin.tm.repository.model.TaskRepository;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.kuzin.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static IProjectRepository projectRepository;

    @NotNull
    private static ITaskRepository taskRepository;

    private static EntityManager entityManager;

    @NotNull
    private static ProjectTaskService projectTaskService;

    public static User USER_1;

    public static User USER_2;

    @NotNull
    private static IUserService userService;

    public static long USER_ID_COUNTER = 0;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskService(connectionService);
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        userService = new UserService(propertyService, connectionService);
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("proj_tsk_serv_mod_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("proj_tsk_serv_mod_usr_2_" + USER_ID_COUNTER, "1");
        entityManager.getTransaction().begin();
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project");
            project.setDescription("Description");
            project.setUser(i == 1 ? USER_1 : USER_2);
            projectRepository.add(project.getUser().getId(), project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUser(USER_1);
            taskRepository.add(USER_1.getId(), task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUser(USER_2);
            taskRepository.add(USER_2.getId(), task);
            taskList.add(task);
        }
        entityManager.getTransaction().commit();
    }

    @After
    public void closeConnection() {
        entityManager.getTransaction().begin();
        taskRepository.clear(USER_1.getId());
        taskRepository.clear(USER_2.getId());
        projectRepository.clear(USER_1.getId());
        projectRepository.clear(USER_2.getId());
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_1.getId(), projectRepository.findOneByIndex(USER_1.getId(), 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final Project project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUser().getId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUser().getId().equals(task.getUser().getId()))
                    projectTaskService.bindTaskToProject(project.getUser().getId(), project.getId(), task.getId());
            }
            List<Task> tasks = taskRepository.findAll(project.getUser().getId());
            for (final Task task : tasks) {
                Assert.assertNotNull(
                        taskRepository.findAllByProjectId(project.getUser().getId(), project.getId())
                                .stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_1.getId(), projectRepository.findOneByIndex(USER_1.getId(), 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUser().getId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUser().getId().equals(task.getUser().getId()))
                    projectTaskService.unbindTaskFromProject(project.getUser().getId(), project.getId(), task.getId());
            }
            List<Task> tasks = taskRepository.findAll(project.getUser().getId());
            for (final Task task : tasks) {
                Assert.assertNull(
                        taskRepository.findAllByProjectId(project.getUser().getId(), project.getId())
                                .stream()
                                .filter(m -> task.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_1.getId(), NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_1.getId(), EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertNotNull(taskRepository.findAllByProjectId(project.getUser().getId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUser().getId()).size());
            projectTaskService.removeProjectById(project.getUser().getId(), project.getId());
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUser().getId(), project.getId()));
            Assert.assertEquals(0, taskRepository.findAll(project.getUser().getId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize(USER_1.getId()) + taskRepository.getSize(USER_2.getId()));
    }

}