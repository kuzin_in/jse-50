package ru.kuzin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.kuzin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> {

    void add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable final Sort sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    int getSize(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

    void update(@NotNull M model);

}