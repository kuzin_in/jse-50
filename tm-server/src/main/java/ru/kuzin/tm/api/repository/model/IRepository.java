package ru.kuzin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractModel;

import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable final Sort sort);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void update(@NotNull M model);

}