package ru.kuzin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.model.ISessionRepository;
import ru.kuzin.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final Session model) {
        entityManager.remove(model);
    }

}