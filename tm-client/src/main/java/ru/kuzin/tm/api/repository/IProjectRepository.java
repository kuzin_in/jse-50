package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.ProjectDTO;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Nullable
    ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @Nullable
    ProjectDTO create(@NotNull String userId, @NotNull String name);

}